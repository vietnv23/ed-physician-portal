import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PATIENT_SERVICE_INJECTION_TOKEN } from '../../../constants/injection-token.constant';
import { IPatientService } from '../../../services/interfaces/patient-service.interface';
import { forkJoin, Observable, of, Subject, Subscription } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { PatientModel } from '../../../view-models/patient-model';
import { ActivatedRoute } from '@angular/router';
import { SearchHeartBeatModel } from '../../../view-models/search-heart-beat.model';
import { HeartBeatModel } from '../../../view-models/heart-beat.model';
import { DateRangePickerComponent } from '../date-range-picker/date-range-picker.component';
import * as moment from 'moment';
import { empty } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'patient-details',
  templateUrl: 'details.component.html',
  styleUrls: ['details.component.scss']
})

export class DetailsComponent implements OnInit, OnDestroy {
  @ViewChild(DateRangePickerComponent)
  private dateRangeComponent: DateRangePickerComponent;

  // subscription handler
  private readonly _subscription: Subscription;

  // search heart beat subject
  private _searchHeartBeatSubject: Subject<SearchHeartBeatModel>;

  // patient id
  private _patientId: string;

  // patient details
  private _patientDetails: PatientModel;
  public get patientDetails(): PatientModel {
    return this._patientDetails;
  }

  // heart beat data
  private _heartBeatsData: HeartBeatModel[];
  public get heartBeatsData(): HeartBeatModel[] {
    return this._heartBeatsData;
  }

  private searchModel = new SearchHeartBeatModel();

  private reloadInterval;

  public disableGoToLiveChart = true;

  public hasError = false;

  constructor(protected route: ActivatedRoute,
    @Inject(PATIENT_SERVICE_INJECTION_TOKEN) protected patientService: IPatientService) {
    this._subscription = new Subscription();
    this._searchHeartBeatSubject = new Subject<SearchHeartBeatModel>();
  }

  private searchHeartBeatSubscription() {
    const subscription = this._searchHeartBeatSubject.pipe(
      mergeMap((searchModel: SearchHeartBeatModel) => {
        return this.patientService.getHeartbeatsDataAsync(searchModel.patientId, searchModel.startDate, searchModel.endDate);
      }),
      tap((heartBeats: HeartBeatModel[]) => {
        this._heartBeatsData = heartBeats;
      }),
    ).subscribe(res => {
      this.hasError = false
    }, err => { this.hasError = true; this._heartBeatsData = []; });
    this._subscription.add(subscription);
  }

  public searchHeartBeat() {
    this.disableGoToLiveChart = false;
    this.searchModel.startDate = moment(this.dateRangeComponent.filterForm.value.dateFrom).format();
    this.searchModel.endDate = moment(this.dateRangeComponent.filterForm.value.dateTo).format();
    if (this.hasError) {
      this.searchHeartBeatSubscription();
    }
    this._searchHeartBeatSubject.next(this.searchModel);
    clearInterval(this.reloadInterval);
  }

  public goToLiveChart() {
    this.disableGoToLiveChart = true;
    this.searchModel.startDate = null;
    this.searchModel.endDate = null;
    if (this.hasError) {
      this.searchHeartBeatSubscription();
    }
    this.reloadInterval = setInterval(() => {
      this._searchHeartBeatSubject.next(this.searchModel);
    }, 3000);
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.params;
    this._patientId = routeParams.id.toString();
    this.searchModel.patientId = this._patientId;

    // search heart beat subject
    this.searchHeartBeatSubscription();

    this.reloadInterval = setInterval(() => {
      this._searchHeartBeatSubject.next(this.searchModel);
    }, 3000);

    // get patient details observable
    const getPatientDetailsSubscription = this.patientService.getPatientDetailsByIdAsync(this._patientId)
      .pipe(
        tap((patient: PatientModel) => {
          this._patientDetails = patient;
          this.searchModel.patientId = patient.id;
          this._searchHeartBeatSubject.next(this.searchModel);
        })
      )
      .subscribe();

    this._subscription.add(getPatientDetailsSubscription);
  }

  ngOnDestroy(): void {
    clearInterval(this.reloadInterval);
    if (this._subscription && !this._subscription.closed) {
      this._subscription.unsubscribe();
    }
  }

}
