import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {ChartDataSets, ChartOptions} from 'chart.js';
import * as moment from 'moment';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import {HeartBeatModel} from '../../../view-models/heart-beat.model';

const MAX_DATA_CHART = 50;

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'heartbeat-chart',
  templateUrl: './heartbeat-chart.component.html',
  styleUrls: []
})
export class HeartBeatChartComponent implements OnInit, OnChanges {
  constructor() {
  }

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;
  @Input() heartBeatDataSouce: HeartBeatModel[];

  private recordedDate = [];
  private heartbeat = [];

  public lineChartData: ChartDataSets[] = [
    {data: this.heartbeat, label: 'ECG', lineTension: 0},
  ];
  public lineChartLabels: Label[] = this.recordedDate;
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        type: 'category',
        ticks: {
          beginAtZero: true,
          autoSkip: false,
          stepSize: 1,
        },
      }],
      yAxes: [{
        gridLines: {
          color: 'rgba(255,0,0,0.3)',
        },
        ticks: {
          max: 150,
          min: 0,
          stepSize: 10,
        }
      }],
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    }
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  private extractChartData = (listHeartBeat: HeartBeatModel[], maxData: number): void => {
    this.heartbeat = [];
    this.recordedDate = [];
    listHeartBeat.forEach((heartBeat: HeartBeatModel) => {
      if (this.heartbeat.length > maxData) {
        this.heartbeat.shift();
        this.recordedDate.shift();
      }
      this.heartbeat.push(heartBeat.heart_rate);
      this.recordedDate.push(moment(heartBeat.recorded_date).format('MM/DD hh:mm:ss'));
    });
    this.lineChartData[0].data = this.heartbeat;
    this.lineChartLabels = this.recordedDate;
  };

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.extractChartData(changes['heartBeatDataSouce'].currentValue, MAX_DATA_CHART);
  }
}
