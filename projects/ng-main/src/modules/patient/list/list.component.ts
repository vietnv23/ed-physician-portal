import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {PATIENT_SERVICE_INJECTION_TOKEN} from '../../../constants/injection-token.constant';
import {IPatientService} from '../../../services/interfaces/patient-service.interface';
import {Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {PatientModel} from '../../../view-models/patient-model';
import {Router} from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'patient-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.scss']
})

export class ListComponent implements OnInit, OnDestroy {
  // subscription handler
  private readonly _subscription: Subscription;

  private _patients: PatientModel[];
  public get patients(): PatientModel[] {
    return this._patients;
  }

  constructor(@Inject(PATIENT_SERVICE_INJECTION_TOKEN) protected patientService: IPatientService, public router: Router) {
    this._subscription = new Subscription();
  }

  ngOnInit(): void {
    const getPatientListSubscription = this.patientService.getPatientsAsync()
      .pipe(
        tap((patients: PatientModel[]) => {
          this._patients = patients;
        })
      )
      .subscribe();
    this._subscription.add(getPatientListSubscription);
  }

  public navigateToPatientDetail(patient: PatientModel): void {
    this.router.navigate(['/patient', patient.id]);
  }

  ngOnDestroy(): void {
    if (this._subscription && !this._subscription.closed) {
      this._subscription.unsubscribe();
    }
  }

}
