import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthenticatedLayoutComponent} from '../shared/authenticated-layout/authenticated-layout.component';
import {IsAuthorizedGuard} from '../../guards/is-authorized-guard';
import {AuthenticatedLayoutModule} from '../shared/authenticated-layout/authenticated-layout.module';
import {ListComponent} from './list/list.component';
import {CommonModule} from '@angular/common';
import {DetailsComponent} from './details/details.component';
import { ChartsModule } from 'ng2-charts';
import { HeartBeatChartComponent } from './heartbeat-chart/heartbeat-chart.component';
import { DpDatePickerModule } from 'ng2-date-picker';
import { DateRangePickerComponent } from './date-range-picker/date-range-picker.component';
import { ReactiveFormsModule } from '@angular/forms';


//#region Route configuration

const routes: Routes = [
  {
    path: '',
    component: AuthenticatedLayoutComponent,
    canActivate: [IsAuthorizedGuard],
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: ':id',
        component: DetailsComponent,
      }
    ]
  }
];


//#endregion

//#region Module configuration

@NgModule({
  imports: [
      AuthenticatedLayoutModule,
      RouterModule.forChild(routes),
      CommonModule,
      ChartsModule,
      DpDatePickerModule,
      ReactiveFormsModule,
  ],
  declarations: [
    ListComponent,
    DetailsComponent,
    HeartBeatChartComponent,
    DateRangePickerComponent,
  ],
  exports: [
    RouterModule
  ]
})
export class PatientRouteModule {
}

//#endregion
