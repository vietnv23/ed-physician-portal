import { Component, TemplateRef } from '@angular/core';
import { ToastModel } from '../../../../view-models/notification.model';
import { ToastService } from './toast-service';


@Component({
    // tslint:disable-next-line: component-selector
    selector: 'toasts-component',
    template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts"
      [header]="toast.textOrTpl"
      [class]="toast.classname"
      [autohide]="true"
      [delay]="toast.delay || 300000"
      (hidden)="toastService.remove(toast)"
    >
      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="toast.textOrTpl"></ng-template>
      </ng-template>

      <ng-template #text>{{ toast.textOrTpl }}</ng-template>
    </ngb-toast>
  `,
    // tslint:disable-next-line: no-host-metadata-property
    host: { '[class.ngb-toasts]': 'true' }
})
export class ToastsContainerComponent {
    constructor(public toastService: ToastService) { }
    isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }
}
