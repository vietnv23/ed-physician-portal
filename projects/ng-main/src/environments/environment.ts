// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDY5AmBo67h7E3icrxY_zQ4MG5TxHeOEJQ",
    authDomain: "fhs-ed-iot.firebaseapp.com",
    projectId: "fhs-ed-iot",
    storageBucket: "fhs-ed-iot.appspot.com",
    messagingSenderId: "587287114726",
    appId: "1:587287114726:web:8d155c374705a38739dc27"
  }
};
