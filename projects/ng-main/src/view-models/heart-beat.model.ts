export class HeartBeatModel {
  id: string;
  recorded_date: string;
  heart_rate: number;
}
