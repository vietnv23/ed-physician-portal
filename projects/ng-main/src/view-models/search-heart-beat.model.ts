export class SearchHeartBeatModel {
  patientId: string;
  startDate: string;
  endDate: string;
}
