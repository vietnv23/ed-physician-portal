import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../models/app-settings';
import {Observable} from 'rxjs';

@Injectable()
export class AppConfigService {

  //#region Properties

  private _appConfiguration: AppSettings;

  //#endregion

  //#region Constructors

  constructor(public httpClient: HttpClient) {

  }

  //#endregion

  //#region Application configuration

  /*
  * Load app configuration from json file.
  * */
  public loadSettingsAsync(): Observable<AppSettings> {
    return this.httpClient.get<AppSettings>('/assets/appsettings.json');
  }

  /*
  * Load configuration from cache.
  * */
  public loadConfigurationFromCache(): AppSettings {
    return this._appConfiguration;
  }

  //#endregion
}
