import {IApiEndPointResolver} from '../interfaces/api-end-point.resolver';
import {Injectable} from '@angular/core';
import {AppConfigService} from './app-config.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppSettings} from '../../models/app-settings';

@Injectable()
export class EndPointResolver implements IApiEndPointResolver {
  // constructor
  public constructor(protected appConfigService: AppConfigService) {
  }

  public getEndPointAsync(moduleName: string): Observable<string> {
    return this.appConfigService
      .loadSettingsAsync()
      .pipe(
        map((appSettings: AppSettings) => {
          return `${appSettings.baseUrl}/${moduleName}`;
        })
      );
  }

}
