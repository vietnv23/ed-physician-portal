import {Observable} from 'rxjs';

export interface IApiEndPointResolver {
  // Get end point about a module.
  getEndPointAsync(moduleName: string): Observable<string>;
}
