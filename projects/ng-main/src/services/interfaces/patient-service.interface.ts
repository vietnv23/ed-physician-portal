import {Observable} from 'rxjs';
import {PatientModel} from '../../view-models/patient-model';
import {HeartBeatModel} from '../../view-models/heart-beat.model';

export interface IPatientService {
  // get all patients from API
  getPatientsAsync(): Observable<PatientModel[]>;

  // get patient details by ID from API
  getPatientDetailsByIdAsync(patientId: string): Observable<PatientModel>;

  // get heartbeats data from API
  getHeartbeatsDataAsync(patientId: string, startDate: string, endDate: string): Observable<HeartBeatModel[]>;

}
