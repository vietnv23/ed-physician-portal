import {ModuleWithProviders, NgModule} from '@angular/core';
import {AccountService} from './implementations/account.service';
import {AuthenticationService} from './implementations/authentication.service';
import {UiService} from './implementations/ui.service';
import {
  API_ENDPOINT_RESOLVER,
  AUTHENTICATION_SERVICE_INJECTION_TOKEN, PATIENT_SERVICE_INJECTION_TOKEN,
  UI_SERVICE_INJECTION_TOKEN,
  USER_SERVICE_INJECTION_TOKEN
} from '../constants/injection-token.constant';
import {EndPointResolver} from './implementations/end-point.resolver';
import {PatientService} from './implementations/patient.service';

@NgModule({})

export class ServiceModule {

  //#region Methods

  static forRoot(): ModuleWithProviders<ServiceModule> {
    return {
      ngModule: ServiceModule,
      providers: [
        {provide: API_ENDPOINT_RESOLVER, useClass: EndPointResolver},
        {provide: USER_SERVICE_INJECTION_TOKEN, useClass: AccountService},
        {provide: AUTHENTICATION_SERVICE_INJECTION_TOKEN, useClass: AuthenticationService},
        {provide: UI_SERVICE_INJECTION_TOKEN, useClass: UiService},
        {provide: PATIENT_SERVICE_INJECTION_TOKEN, useClass: PatientService},
      ]
    };
  }

  //#endregion
}


